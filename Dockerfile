FROM golang:1-alpine AS build

RUN apk add --update --no-cache git

ADD . /go/src/implementation-test-process/
WORKDIR /go/src/implementation-test-process
RUN go mod download
RUN go build -o dist/bin/implementation-test-process ./cmd/implementation-test-process

# Release binary on latest alpine image.
FROM alpine:latest

COPY --from=build /go/src/implementation-test-process/dist/bin/implementation-test-process /usr/local/bin/implementation-test-process

# Add non-priveleged user. Disabled for openshift
RUN adduser -D -u 1001 appuser
USER appuser
CMD ["/usr/local/bin/implementation-test-process"]
