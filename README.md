# implementation-test-process

```sh
kubectl create namespace bk-implementation-test

skaffold dev -f=skaffold.yaml -p dev
```

Use postman or other REST tool

Header:

```http
POST /v1/test/ HTTP/1.1
Host: implementation-test-process.vorderingenoverzicht.blauweknop.bk:80
```

Body

```json
{
  "discoveryUrl": "https://cjib.vorijk-demo.blauweknop.app/.well-known/bk-configuration.json",
  "bsn": "814859094",
  "publicKey": "-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEG3EluTea7Tyj4xKqWdBYjcFj8HsQ\ngTxS1phFm/3wMnOWxUev5rNhYvW07RSepi8DbpcFBbDWfzi1XQle1F1Mgg==\n-----END PUBLIC KEY-----\n",
  "schemeDiscoveryUrl": "https://api.stelsel.vorijk-demo.blauweknop.app/v1",
  "appManagerOin": "00000001001172773000"
}
```
