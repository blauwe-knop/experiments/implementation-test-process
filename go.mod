module gitlab.com/blauwe-knop/experiments/implementation-test-process

go 1.22.0

toolchain go1.22.1

require (
	github.com/go-chi/chi/v5 v5.0.12
	github.com/go-chi/cors v1.2.1
	github.com/golang-jwt/jwt/v5 v5.2.1
	github.com/jessevdk/go-flags v1.5.0
	gitlab.com/blauwe-knop/common/bk-crypto v0.0.2
	gitlab.com/blauwe-knop/common/health-checker v0.0.4
	gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process v0.16.1
	gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service v0.16.0
	gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process v0.16.3
	gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system v0.16.0
	gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process v0.16.1
	gitlab.com/blauwe-knop/vorderingenoverzicht/service-discovery-process v0.15.3
	gitlab.com/blauwe-knop/vorderingenoverzicht/session-process v0.16.1
	go.uber.org/zap v1.27.0
)

require (
	github.com/btcsuite/btcd/btcec/v2 v2.3.3 // indirect
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.3.0 // indirect
	github.com/ethereum/go-ethereum v1.14.3 // indirect
	github.com/holiman/uint256 v1.2.4 // indirect
	gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service v0.16.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/crypto v0.23.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
)
