// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type TestInfo struct {
	DiscoveryUrl       string `json:"discoveryUrl"`
	Bsn                string `json:"bsn"`
	PublicKey          string `json:"publicKey"`
	SchemeDiscoveryUrl string `json:"schemeDiscoveryUrl"`
	AppManagerOin      string `json:"appManagerOin"`
}
