// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repository_log_wrappers

import (
	"fmt"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/model"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/repositories"
	"go.uber.org/zap"
)

type SchemeTestHelper struct {
	SchemeRepository repositories.SchemeRepository
	Logger           *zap.Logger
}

func NewSchemeTestHelper(logger *zap.Logger, schemeApiUrl string) *SchemeTestHelper {
	logger.Debug("create schemeRepository", zap.String("schemeApiUrl", schemeApiUrl))
	schemeRepository := repositories.NewSchemeClient(
		schemeApiUrl,
	)

	return &SchemeTestHelper{
		Logger:           logger,
		SchemeRepository: schemeRepository,
	}
}

func (uc *SchemeTestHelper) FetchAppManager(appManagerOin string) (*model.AppManager, error) {
	uc.Logger.Debug("schemeRepository.FetchAppManager", zap.String("appManagerOin", appManagerOin))
	appManager, err := uc.SchemeRepository.FetchAppManager(appManagerOin)
	if err != nil {
		uc.Logger.Error("SchemeRepository.FetchAppManager failed", zap.Error(err))
		return nil, fmt.Errorf("SchemeRepository.FetchAppManager failed: %v", err)
	}

	uc.Logger.Info("SchemeRepository.FetchAppManager succeeded", zap.Reflect("appManager", appManager))

	return appManager, nil
}

func (uc *SchemeTestHelper) CheckSchemeApiHealth() error {
	panic("CheckSchemeApiHealth not implemented")
	// 	uc.Logger.Debug("schemeRepository.GetHealth")
	// 	err := uc.SchemeRepository.GetHealth()
	// 	if err != nil {
	// 		uc.Logger.Error("SchemeRepository.GetHealth failed", zap.Error(err))
	// 		return healthCheckResult, fmt.Errorf("SchemeRepository.GetHealth failed: %v", err)
	// 	}

	// 	uc.Logger.Info("SchemeRepository.GetHealth succeeded")

	// return healthCheckResult, nil
}

func (uc *SchemeTestHelper) CheckSchemeApiHealthCheck() (healthcheck.Result, error) {
	panic("CheckSchemeApiHealthCheck not implemented")
	// 	uc.Logger.Debug("SchemeRepository.GetHealthCheck")
	// 	healthCheckResult := uc.SchemeRepository.GetHealthCheck()
	// 	if healthCheckResult.Status == healthcheck.StatusError {
	// 		uc.Logger.Error("SchemeRepository.GetHealthCheck failed", zap.Reflect("healthCheckResult", healthCheckResult))
	// 		return fmt.Errorf("SchemeRepository.GetHealthCheck failed: %v", healthCheckResult)
	// 	}

	// 	uc.Logger.Info("SchemeRepository.GetHealthCheck succeeded", zap.Reflect("healthCheckResult", healthCheckResult))

	// return nil
}
