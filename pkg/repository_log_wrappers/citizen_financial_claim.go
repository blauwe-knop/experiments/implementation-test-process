// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repository_log_wrappers

import (
	"fmt"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/pkg/model"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/pkg/repositories"
	"go.uber.org/zap"
)

type CitizenFinancialClaimRepositoryLogWrapper struct {
	CitizenFinancialClaimRepository repositories.CitizenFinancialClaimRepository
	Logger                          *zap.Logger
}

func NewCitizenFinancialClaimRepositoryLogWrapper(zapLogger *zap.Logger, financialClaimRequestApiUrl string) *CitizenFinancialClaimRepositoryLogWrapper {
	zapLogger.Debug("create citizenFinancialClaimRepository", zap.String("financialClaimRequestApiUrl", financialClaimRequestApiUrl))
	citizenFinancialClaimRepository := repositories.NewCitizenFinancialClaimClient(
		financialClaimRequestApiUrl,
	)

	return &CitizenFinancialClaimRepositoryLogWrapper{
		Logger:                          zapLogger,
		CitizenFinancialClaimRepository: citizenFinancialClaimRepository,
	}
}

func (uc *CitizenFinancialClaimRepositoryLogWrapper) ConfigureClaimsRequest(sessionToken string, configurationRequest model.ConfigurationRequest) (*model.Configuration, error) {
	uc.Logger.Log(zap.DebugLevel, "CitizenFinancialClaimRepository.ConfigureClaimsRequest",
		zap.String("sessionToken", sessionToken),
		zap.Reflect("configurationRequest", configurationRequest),
	)
	configuration, err := uc.CitizenFinancialClaimRepository.ConfigureClaimsRequest(
		sessionToken,
		configurationRequest,
	)
	if err != nil {
		uc.Logger.Log(zap.ErrorLevel, "CitizenFinancialClaimRepository.ConfigureClaimsRequest failed", zap.Error(err))
		return nil, fmt.Errorf("CitizenFinancialClaimRepository.ConfigureClaimsRequest failed: %v", err)
	}

	uc.Logger.Log(zap.InfoLevel, "CitizenFinancialClaimRepository.ConfigureClaimsRequest succeeded", zap.Reflect("configuration", configuration))

	return configuration, nil
}

func (uc *CitizenFinancialClaimRepositoryLogWrapper) RequestFinancialClaimsInformation(sessionToken string, configurationToken string) (*model.FinancialClaimsInformation, error) {
	uc.Logger.Log(zap.DebugLevel, "CitizenFinancialClaimRepository.RequestFinancialClaimsInformation",
		zap.String("sessionToken", sessionToken),
		zap.Reflect("configurationToken", configurationToken),
	)
	financialClaimsInformation, err := uc.CitizenFinancialClaimRepository.RequestFinancialClaimsInformation(
		sessionToken,
		configurationToken,
	)
	if err != nil {
		uc.Logger.Log(zap.ErrorLevel, "CitizenFinancialClaimRepository.RequestFinancialClaimsInformation failed", zap.Error(err))
		return nil, fmt.Errorf("CitizenFinancialClaimRepository.RequestFinancialClaimsInformation failed: %v", err)
	}

	uc.Logger.Log(zap.InfoLevel, "CitizenFinancialClaimRepository.RequestFinancialClaimsInformation succeeded", zap.Reflect("financialClaimsInformation", financialClaimsInformation))

	return financialClaimsInformation, nil
}

func (uc *CitizenFinancialClaimRepositoryLogWrapper) CheckFinancialClaimRequestApiHealth() error {
	uc.Logger.Log(zap.DebugLevel, "citizenFinancialClaimRepository.GetHealth")
	err := uc.CitizenFinancialClaimRepository.GetHealth()
	if err != nil {
		uc.Logger.Log(zap.ErrorLevel, "citizenFinancialClaimRepository.GetHealth failed", zap.Error(err))
		return fmt.Errorf("citizenFinancialClaimRepository.GetHealth failed: %v", err)
	}

	uc.Logger.Log(zap.InfoLevel, "citizen financial claim process - online")

	return nil
}

func (uc *CitizenFinancialClaimRepositoryLogWrapper) CheckFinancialClaimRequestApiHealthCheck() (healthcheck.Result, error) {
	uc.Logger.Log(zap.DebugLevel, "CitizenFinancialClaimRepository.GetHealthCheck")
	healthCheckResult := uc.CitizenFinancialClaimRepository.GetHealthCheck()
	if healthCheckResult.Status == healthcheck.StatusError {
		uc.Logger.Log(zap.ErrorLevel, "CitizenFinancialClaimRepository.GetHealthCheck failed", zap.Reflect("healthCheckResult", healthCheckResult))
		return healthCheckResult, fmt.Errorf("CitizenFinancialClaimRepository.GetHealthCheck failed: %v", healthCheckResult)
	}

	uc.Logger.Log(zap.InfoLevel, "CitizenFinancialClaimRepository.GetHealthCheck succeeded", zap.Reflect("healthCheckResult", healthCheckResult))

	return healthCheckResult, nil
}
