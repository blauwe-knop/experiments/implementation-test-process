// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package helpers

import (
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/pkg/model"

	"gitlab.com/blauwe-knop/experiments/implementation-test-process/pkg/repository_log_wrappers"
)

func StartSession(
	cryptographyTestHelper *CryptographyTestHelper,
	sessionTestHelper *repository_log_wrappers.SessionTestHelper,
	appPublicKey string,
	appPrivateKey string,
	organizationPublicKey string,
) (string, error) {
	challengeRequestMessage := model.ChallengeRequestMessage{
		AppPublicKey: appPublicKey,
	}

	encryptedChallengeRequestMessage, err := cryptographyTestHelper.EncryptChallengeRequestMessage(
		challengeRequestMessage,
		organizationPublicKey,
	)
	if err != nil {
		return "", err
	}

	challengeRequest := model.ChallengeRequest{
		Request: encryptedChallengeRequestMessage,
	}

	challenge, err := sessionTestHelper.CreateChallenge(
		challengeRequest,
	)
	if err != nil {
		return "", err
	}

	err = cryptographyTestHelper.VerifyNonce(
		*challenge,
		organizationPublicKey,
	)
	if err != nil {
		return "", err
	}

	signature, err := cryptographyTestHelper.SignNonce(
		challenge.Nonce,
		appPrivateKey,
	)
	if err != nil {
		return "", err
	}

	challengeResult := model.ChallengeResult{
		AppSignature: signature,
	}

	encryptedChallengeResult, err := cryptographyTestHelper.EncryptChallengeResult(
		challengeResult,
		organizationPublicKey,
	)
	if err != nil {
		return "", err
	}

	challengeResponse := model.ChallengeResponse{
		Nonce:    challenge.Nonce,
		Response: encryptedChallengeResult,
	}

	session, err := sessionTestHelper.CompleteChallenge(
		challengeResponse,
	)
	if err != nil {
		return "", err
	}

	return session.Token, nil
}
