// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package helpers

import (
	"gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"

	"gitlab.com/blauwe-knop/experiments/implementation-test-process/pkg/repository_log_wrappers"
)

func UnregisterApp(
	appManagementRepositoryLogWrapper *repository_log_wrappers.AppManagementRepositoryLogWrapper,
	cryptographyTestHelper *CryptographyTestHelper,
	appManagementSessionTestHelper *repository_log_wrappers.SessionTestHelper,
	appKeyPair *KeyPair,
	appManagerPublicKey string,
	registrationToken string,
) error {
	signature, err := cryptographyTestHelper.SignRegistrationToken(
		registrationToken,
		appKeyPair.PrivateKey,
	)
	if err != nil {
		return err
	}

	appManagerSessionToken, err := StartSession(
		cryptographyTestHelper,
		appManagementSessionTestHelper,
		appKeyPair.PublicKey,
		appKeyPair.PrivateKey,
		appManagerPublicKey,
	)
	if err != nil {
		return err
	}

	unregisterAppRequest := model.UnregisterAppRequest{
		RegistrationToken: registrationToken,
		Signature:         signature,
	}

	err = appManagementRepositoryLogWrapper.UnregisterApp(
		appManagerSessionToken,
		unregisterAppRequest,
	)
	if err != nil {
		return err
	}

	return err
}
