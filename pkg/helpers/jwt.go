// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package helpers

import (
	"fmt"

	"github.com/golang-jwt/jwt/v5"
	"go.uber.org/zap"

	bk_crypto "gitlab.com/blauwe-knop/common/bk-crypto"
)

type JwtTestHelper struct {
	Logger *zap.Logger
}

func NewJwtTestHelper(logger *zap.Logger) *JwtTestHelper {
	return &JwtTestHelper{
		Logger: logger,
	}
}

func (uc *JwtTestHelper) CreateClaims(
	appPublicKey string,
	appManagerOin string,
	appManagerPublicKey string,
	givenName string,
	familyName string,
	birthdate string,
	bsn string,
	issuedAt int64,
	notBefore int64,
	expirationTime int64,
) jwt.MapClaims {
	uc.Logger.Debug("make claims",
		zap.String("app_public_key", appPublicKey),
		zap.String("app_manager_oin", appManagerOin),
		zap.String("app_manager_public_key", appManagerPublicKey),
		zap.String("given_name", givenName),
		zap.String("family_name", familyName),
		zap.String("birthdate", birthdate),
		zap.String("bsn", bsn),
		zap.Int64("iat", issuedAt),
		zap.Int64("nbf", notBefore),
		zap.Int64("exp", expirationTime),
	)

	claims := make(jwt.MapClaims)
	claims["app_public_key"] = appPublicKey
	claims["app_manager_oin"] = appManagerOin
	claims["app_manager_public_key"] = appManagerPublicKey
	claims["given_name"] = givenName
	claims["family_name"] = familyName
	claims["birthdate"] = birthdate
	claims["bsn"] = bsn
	claims["iat"] = issuedAt
	claims["nbf"] = notBefore
	claims["exp"] = expirationTime

	uc.Logger.Info("make claims succeeded", zap.Reflect("claims", claims))

	return claims
}

func (uc *JwtTestHelper) CreateAccessToken(claims jwt.MapClaims, appManagerPrivateKey string) (string, error) {
	uc.Logger.Debug("DecodePrivateKeyFromPem",
		zap.String("appManagerPrivateKey", appManagerPrivateKey),
	)
	ecPrivateKey, err := bk_crypto.DecodePrivateKeyFromPem(appManagerPrivateKey)
	if err != nil {
		uc.Logger.Error("DecodePrivateKeyFromPem failed", zap.Error(err))
		return "", fmt.Errorf("DecodePrivateKeyFromPem failed: %v", err)
	}

	uc.Logger.Debug("create access token",
		zap.Reflect("claims", claims),
		zap.Reflect("ecPrivateKey", ecPrivateKey),
	)

	accessToken, err := jwt.NewWithClaims(jwt.SigningMethodES256, claims).SignedString(ecPrivateKey)
	if err != nil {
		uc.Logger.Error("create access token failed", zap.Error(err))
		return "", fmt.Errorf("create access token failed: %w", err)
	}

	uc.Logger.Info("create access token succeeded", zap.Reflect("claims", claims))

	return accessToken, nil
}
