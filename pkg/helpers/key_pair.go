// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package helpers

type KeyPair struct {
	PublicKey  string
	PrivateKey string
}
