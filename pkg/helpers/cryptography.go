// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package helpers

import (
	"encoding/base64"
	"encoding/json"
	"fmt"

	bk_crypto "gitlab.com/blauwe-knop/common/bk-crypto"
	appManagementProcessModel "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"
	citizenFinancialClaimModel "gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/pkg/model"
	sourceSystemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
	sessionModel "gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/pkg/model"

	"go.uber.org/zap"
)

type CryptographyTestHelper struct {
	Logger *zap.Logger
}

func NewCryptographyTestHelper(logger *zap.Logger) *CryptographyTestHelper {
	return &CryptographyTestHelper{
		Logger: logger,
	}
}

func (uc *CryptographyTestHelper) GenerateKeyPair() (*KeyPair, error) {
	uc.Logger.Debug("generate key pair")
	privateKey, err := bk_crypto.GenerateKeyPair()
	if err != nil {
		uc.Logger.Error("generate key pair failed", zap.Error(err))
		return nil, fmt.Errorf("generate key pair failed: %v", err)
	}

	uc.Logger.Info("generate key pair succeeded", zap.Reflect("privateKey", privateKey))

	privateKeyPem, err := bk_crypto.EncodePrivateKeyToPem(privateKey)
	if err != nil {
		uc.Logger.Error("EncodePrivateKeyToPem failed", zap.Error(err))
		return nil, fmt.Errorf("EncodePrivateKeyToPem failed: %v", err)
	}

	publicKeyPem, err := bk_crypto.EncodePublicKeyToPem(&privateKey.PublicKey)
	if err != nil {
		uc.Logger.Error("EncodePublicKeyToPem failed", zap.Error(err))
		return nil, fmt.Errorf("EncodePublicKeyToPem failed: %v", err)
	}

	return &KeyPair{
		PublicKey:  publicKeyPem,
		PrivateKey: privateKeyPem,
	}, nil
}

func (uc *CryptographyTestHelper) EncryptChallengeRequestMessage(challengeRequestMessage sessionModel.ChallengeRequestMessage, organizationPublicKey string) (string, error) {
	uc.Logger.Debug("json.Marshal", zap.Reflect("challengeRequestMessage", challengeRequestMessage))
	challengeRequestMessageAsJson, err := json.Marshal(challengeRequestMessage)
	if err != nil {
		return "", fmt.Errorf("json.Marshal challengeRequestMessage failed: %v", err)
	}

	uc.Logger.Debug("bk_crypto.DecodePublicKeyFromPem", zap.String("organizationPublicKey", organizationPublicKey))
	ecOrganizationPublicKey, err := bk_crypto.DecodePublicKeyFromPem(organizationPublicKey)
	if err != nil {
		return "", fmt.Errorf("DecodePublicKeyFromPem organizationPublicKey failed: %v", err)
	}

	uc.Logger.Debug("encrypt",
		zap.Reflect("ecOrganizationPublicKey", ecOrganizationPublicKey),
		zap.String("challengeRequestMessage", string(challengeRequestMessageAsJson)),
	)
	encryptedChallengeRequestMessage, err := bk_crypto.Encrypt(
		ecOrganizationPublicKey,
		challengeRequestMessageAsJson,
	)
	if err != nil {
		uc.Logger.Error("encrypt challengeRequestMessage failed", zap.Error(err))
		return "", fmt.Errorf("encrypt challengeRequestMessage failed: %v", err)
	}

	uc.Logger.Info("encrypt challengeRequestMessage succeeded", zap.String("encryptedChallengeRequestMessage", string(encryptedChallengeRequestMessage)))

	return base64.StdEncoding.EncodeToString(encryptedChallengeRequestMessage), nil
}

func (uc *CryptographyTestHelper) VerifyNonce(challenge sessionModel.Challenge, organizationPublicKey string) error {
	uc.Logger.Debug("bk_crypto.DecodePublicKeyFromPem", zap.String("organizationPublicKey", organizationPublicKey))
	ecOrganizationPublicKey, err := bk_crypto.DecodePublicKeyFromPem(organizationPublicKey)
	if err != nil {
		return fmt.Errorf("DecodePublicKeyFromPem organizationPublicKey failed: %v", err)
	}

	uc.Logger.Debug("verify",
		zap.Reflect("ecOrganizationPublicKey", ecOrganizationPublicKey),
		zap.String("challenge.Nonce", challenge.Nonce),
		zap.String("challenge.Signature", challenge.Signature),
	)
	isVerified, err := bk_crypto.Verify(
		ecOrganizationPublicKey,
		challenge.Nonce,
		challenge.Signature,
	)
	if err != nil {
		uc.Logger.Error("verify nonce failed", zap.Error(err))
		return fmt.Errorf("verify nonce failed: %v", err)
	}

	if !isVerified {
		uc.Logger.Error("verify nonce failed", zap.Bool("isVerified", isVerified))
		return fmt.Errorf("verify nonce failed")
	}

	uc.Logger.Info("verify nonce succeeded")

	return err
}

func (uc *CryptographyTestHelper) SignNonce(nonce string, appPrivateKey string) (string, error) {
	uc.Logger.Debug("bk_crypto.DecodePrivateKeyFromPem", zap.String("appPrivateKey", appPrivateKey))
	ecAppPrivateKey, err := bk_crypto.DecodePrivateKeyFromPem(appPrivateKey)
	if err != nil {
		return "", fmt.Errorf("DecodePublicKeyFromPem appPrivateKey failed: %v", err)
	}

	uc.Logger.Debug("sign",
		zap.String("appPrivateKey", appPrivateKey),
		zap.String("nonce", nonce),
	)
	signature, err := bk_crypto.Sign(
		ecAppPrivateKey,
		nonce,
	)
	if err != nil {
		uc.Logger.Error("sign nonce failed", zap.Error(err))
		return "", fmt.Errorf("sign nonce failed: %v", err)
	}

	uc.Logger.Info("sign nonce succeeded", zap.String("signature", signature))

	return signature, nil
}

func (uc *CryptographyTestHelper) EncryptChallengeResult(challengeResult sessionModel.ChallengeResult, organizationPublicKey string) (string, error) {
	uc.Logger.Debug("bk_crypto.DecodePublicKeyFromPem", zap.String("organizationPublicKey", organizationPublicKey))
	ecOrganizationPublicKey, err := bk_crypto.DecodePublicKeyFromPem(organizationPublicKey)
	if err != nil {
		return "", fmt.Errorf("DecodePublicKeyFromPem organizationPublicKey failed: %v", err)
	}

	uc.Logger.Debug("json.Marshal", zap.Reflect("challengeResult", challengeResult))
	challengeResultAsJson, err := json.Marshal(challengeResult)
	if err != nil {
		return "", fmt.Errorf("json.Marshal challengeResult failed: %v", err)
	}

	uc.Logger.Debug("encrypt",
		zap.String("organizationPublicKey", organizationPublicKey),
		zap.String("challengeResult", string(challengeResultAsJson)),
	)
	encryptedChallengeResult, err := bk_crypto.Encrypt(
		ecOrganizationPublicKey,
		challengeResultAsJson,
	)
	if err != nil {
		uc.Logger.Error("encrypt challengeResult failed", zap.Error(err))
		return "", fmt.Errorf("encrypt challengeResult failed: %v", err)
	}

	uc.Logger.Info("encrypt challengeResult succeeded", zap.String("encryptedChallengeResult", string(encryptedChallengeResult)))

	return base64.StdEncoding.EncodeToString(encryptedChallengeResult), nil
}

func (uc *CryptographyTestHelper) EncryptAppIdentity(appIdentity appManagementProcessModel.AppIdentity, appManagerPublicKey string) (string, error) {
	uc.Logger.Debug("json.Marshal", zap.Reflect("appIdentity", appIdentity))
	appIdentityAsJson, err := json.Marshal(appIdentity)
	if err != nil {
		uc.Logger.Error("json.Marshal appIdentity failed", zap.Error(err))
		return "", fmt.Errorf("json.Marshal appIdentity failed: %v", err)
	}

	uc.Logger.Debug("bk_crypto.DecodePublicKeyFromPem", zap.String("appManagerPublicKey", appManagerPublicKey))
	ecAppManagerPublicKey, err := bk_crypto.DecodePublicKeyFromPem(appManagerPublicKey)
	if err != nil {
		return "", fmt.Errorf("DecodePublicKeyFromPem appManagerPublicKey failed: %v", err)
	}

	uc.Logger.Debug("encrypt",
		zap.Reflect("appManagerPublicKey", ecAppManagerPublicKey),
		zap.String("appIdentity", string(appIdentityAsJson)),
	)
	encryptedAppIdentity, err := bk_crypto.Encrypt(
		ecAppManagerPublicKey,
		appIdentityAsJson,
	)
	if err != nil {
		uc.Logger.Error("encrypt AppIdentity failed", zap.Error(err))
		return "", fmt.Errorf("encrypt AppIdentity failed: %v", err)
	}

	uc.Logger.Info("encrypt AppIdentity succeeded", zap.ByteString("encryptedAppIdentity", encryptedAppIdentity))

	return base64.StdEncoding.EncodeToString(encryptedAppIdentity), nil
}

func (uc *CryptographyTestHelper) DecryptCertificate(encryptedCertificate string, appPrivateKey string) (*appManagementProcessModel.Certificate, error) {
	uc.Logger.Debug("bk_crypto.DecodePrivateKeyFromPem", zap.String("appPrivateKey", appPrivateKey))
	ecAppPrivateKey, err := bk_crypto.DecodePrivateKeyFromPem(appPrivateKey)
	if err != nil {
		return nil, fmt.Errorf("DecodePublicKeyFromPem appPrivateKey failed: %v", err)
	}

	uc.Logger.Debug("decrypt",
		zap.String("appPrivateKey", appPrivateKey),
		zap.String("encryptedCertificate", encryptedCertificate),
	)
	decryptedCertificate, err := bk_crypto.Decrypt(
		ecAppPrivateKey,
		[]byte(encryptedCertificate),
	)
	if err != nil {
		uc.Logger.Error("decrypt Certificate failed", zap.Error(err))
		return nil, fmt.Errorf("decrypt Certificate failed: %v", err)
	}

	uc.Logger.Debug("json.Unmarshal",
		zap.String("decryptedCertificate", string(decryptedCertificate)),
	)
	var certificate appManagementProcessModel.Certificate
	err = json.Unmarshal([]byte(decryptedCertificate), &certificate)
	if err != nil {
		uc.Logger.Error("json.Unmarshal decryptedCertificate failed", zap.Error(err))
		return nil, fmt.Errorf("json.Unmarshal decryptedCertificate failed: %v", err)
	}

	uc.Logger.Info("decrypt Certificate succeeded")
	// disabled due to bytes log , zap.Reflect("certificate", certificate))

	return &certificate, nil
}

func (uc *CryptographyTestHelper) SignDocument(document citizenFinancialClaimModel.Document, appPrivateKey string) (string, error) {
	uc.Logger.Debug("json.Marshal", zap.Reflect("document", document))
	documentAsJson, err := json.Marshal(document)
	if err != nil {
		uc.Logger.Error("json.Marshal document failed", zap.Error(err))
		return "", fmt.Errorf("json.Marshal document failed: %v", err)
	}

	uc.Logger.Debug("bk_crypto.DecodePrivateKeyFromPem", zap.String("appPrivateKey", appPrivateKey))
	ecAppPrivateKey, err := bk_crypto.DecodePrivateKeyFromPem(appPrivateKey)
	if err != nil {
		return "", fmt.Errorf("DecodePublicKeyFromPem appPrivateKey failed: %v", err)
	}

	uc.Logger.Debug("sign",
		zap.String("appPrivateKey", appPrivateKey),
		zap.String("document", string(documentAsJson)),
	)
	signature, err := bk_crypto.Sign(
		ecAppPrivateKey,
		string(documentAsJson),
	)
	if err != nil {
		uc.Logger.Error("sign document failed", zap.Error(err))
		return "", fmt.Errorf("sign document failed: %v", err)
	}

	uc.Logger.Info("sign document succeeded", zap.String("signature", signature))

	return signature, nil
}

func (uc *CryptographyTestHelper) EncryptEnvelope(envelope citizenFinancialClaimModel.Envelope, organizationPublicKey string) (string, error) {
	uc.Logger.Debug("json.Marshal envelope")
	// disabled due to bytes log , zap.Reflect("certificate", certificate))
	envelopeAsJson, err := json.Marshal(envelope)
	if err != nil {
		uc.Logger.Error("json.Marshal envelope failed", zap.Error(err))
		return "", fmt.Errorf("json.Marshal envelope failed: %v", err)
	}

	uc.Logger.Debug("bk_crypto.DecodePublicKeyFromPem", zap.String("organizationPublicKey", organizationPublicKey))
	ecOrganizationPublicKey, err := bk_crypto.DecodePublicKeyFromPem(organizationPublicKey)
	if err != nil {
		return "", fmt.Errorf("DecodePublicKeyFromPem organizationPublicKey failed: %v", err)
	}

	uc.Logger.Debug("encrypt",
		zap.String("organizationPublicKey", organizationPublicKey),
		zap.String("envelope", string(envelopeAsJson)),
	)
	encryptedEnvelope, err := bk_crypto.Encrypt(
		ecOrganizationPublicKey,
		envelopeAsJson,
	)
	if err != nil {
		uc.Logger.Error("encrypt Envelope failed", zap.Error(err))
		return "", fmt.Errorf("encrypt Envelope failed: %v", err)
	}

	uc.Logger.Info("encrypt Envelope succeeded", zap.String("encryptedEnvelope", string(encryptedEnvelope)))

	return base64.StdEncoding.EncodeToString(encryptedEnvelope), nil
}

func (uc *CryptographyTestHelper) DecryptFinancialClaimsInformationDocument(encryptedFinancialClaimsInformationDocument string, appPrivateKey string) (*sourceSystemModel.FinancialClaimsInformationDocument, error) {
	uc.Logger.Debug("bk_crypto.DecodePrivateKeyFromPem", zap.String("appPrivateKey", appPrivateKey))
	ecAppPrivateKey, err := bk_crypto.DecodePrivateKeyFromPem(appPrivateKey)
	if err != nil {
		return nil, fmt.Errorf("DecodePublicKeyFromPem appPrivateKey failed: %v", err)
	}

	uc.Logger.Debug("decrypt",
		zap.String("appPrivateKey", appPrivateKey),
		zap.String("encryptedFinancialClaimsInformationDocument", encryptedFinancialClaimsInformationDocument),
	)
	decryptedFinancialClaimsInformationDocument, err := bk_crypto.Decrypt(
		ecAppPrivateKey,
		[]byte(encryptedFinancialClaimsInformationDocument),
	)
	if err != nil {
		uc.Logger.Error("failed to decrypt financialClaimsInformation with app private key", zap.Error(err))
		return nil, fmt.Errorf("failed to decrypt financialClaimsInformation with app private key: %v", err)
	}

	uc.Logger.Debug("json.Unmarshal decryptedFinancialClaimsInformationDocument",
		zap.String("decryptedFinancialClaimsInformationDocument", string(decryptedFinancialClaimsInformationDocument)),
	)
	var sourceFinancialClaimsInformationDocument sourceSystemModel.FinancialClaimsInformationDocument
	err = json.Unmarshal([]byte(decryptedFinancialClaimsInformationDocument), &sourceFinancialClaimsInformationDocument)
	if err != nil {
		uc.Logger.Error("failed to json unmarshal decryptedFinancialClaimsInformationDocument", zap.Error(err))
		return nil, fmt.Errorf("failed to json unmarshal decryptedFinancialClaimsInformationDocument: %v", err)
	}

	uc.Logger.Info("decrypt FinancialClaimsInformationDocument succeeded", zap.Reflect("sourceFinancialClaimsInformationDocument", sourceFinancialClaimsInformationDocument))

	return &sourceFinancialClaimsInformationDocument, nil
}

func (uc *CryptographyTestHelper) SignRegistrationToken(registrationToken string, appPrivateKey string) (string, error) {
	uc.Logger.Debug("bk_crypto.DecodePrivateKeyFromPem", zap.String("appPrivateKey", appPrivateKey))
	ecAppPrivateKey, err := bk_crypto.DecodePrivateKeyFromPem(appPrivateKey)
	if err != nil {
		return "", fmt.Errorf("DecodePublicKeyFromPem appPrivateKey failed: %v", err)
	}

	uc.Logger.Debug("sign",
		zap.String("appKeyPair.PrivateKey", appPrivateKey),
		zap.String("registrationToken", registrationToken),
	)
	signature, err := bk_crypto.Sign(
		ecAppPrivateKey,
		registrationToken,
	)
	if err != nil {
		uc.Logger.Error("sign registrationToken failed", zap.Error(err))
		return "", fmt.Errorf("sign registrationToken failed: %v", err)
	}

	uc.Logger.Info("sign registrationToken succeeded", zap.String("signature", signature))

	return signature, nil
}
