// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	appManagementProcessModel "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"
	citizenFinancialClaimModel "gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/pkg/model"
	sourceSystemModel "gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"

	"gitlab.com/blauwe-knop/experiments/implementation-test-process/pkg/helpers"
	"gitlab.com/blauwe-knop/experiments/implementation-test-process/pkg/model"
	"gitlab.com/blauwe-knop/experiments/implementation-test-process/pkg/repository_log_wrappers"

	"go.uber.org/zap"
)

type ImplementationTestUsecase struct {
	Logger *zap.Logger
}

func NewImplementationTestUsecase(
	logger *zap.Logger,
) *ImplementationTestUsecase {
	return &ImplementationTestUsecase{
		Logger: logger,
	}
}

func (uc *ImplementationTestUsecase) FunctionalOnlineTest(
	testInfo model.TestInfo,
) (*sourceSystemModel.FinancialClaimsInformationDocument, error) {
	schemeTestHelper := repository_log_wrappers.NewSchemeTestHelper(uc.Logger, testInfo.SchemeDiscoveryUrl)

	appManager, err := schemeTestHelper.FetchAppManager(testInfo.AppManagerOin)
	if err != nil {
		return nil, err
	}

	uc.Logger.Info("setup appManagerServiceDiscoveryTestHelper")
	appManagerServiceDiscoveryTestHelper := repository_log_wrappers.NewServiceDiscoveryTestHelper(uc.Logger, appManager.DiscoveryUrl)

	appManagerDiscoveredServices, err := appManagerServiceDiscoveryTestHelper.FetchDiscoveredServices()
	if err != nil {
		return nil, err
	}

	err = appManagerServiceDiscoveryTestHelper.CheckDiscoveredServices(appManagerDiscoveredServices)
	if err != nil {
		return nil, err
	}

	cryptographyTestHelper := helpers.NewCryptographyTestHelper(uc.Logger)

	appKeyPair, err := cryptographyTestHelper.GenerateKeyPair()
	if err != nil {
		return nil, err
	}

	appIdentity := appManagementProcessModel.AppIdentity{
		AppPublicKey: appKeyPair.PublicKey,
		ClientId:     "source_organization_implementation_test",
	}

	encryptedAppIdentity, err := cryptographyTestHelper.EncryptAppIdentity(appIdentity, appManager.PublicKey)
	if err != nil {
		return nil, err
	}

	registerAppRequest := appManagementProcessModel.RegisterAppRequest{
		EncryptedAppIdentity: encryptedAppIdentity,
	}

	uc.Logger.Debug("setup appManagerSessionTestHelper")
	appManagerSessionTestHelper := repository_log_wrappers.NewSessionTestHelper(uc.Logger, appManagerDiscoveredServices.SessionApi.V1)

	uc.Logger.Debug("appManager startSession")
	appManagerSessionToken, err := helpers.StartSession(
		cryptographyTestHelper,
		appManagerSessionTestHelper,
		appKeyPair.PublicKey,
		appKeyPair.PrivateKey,
		appManager.PublicKey,
	)
	if err != nil {
		return nil, err
	}

	err = appManagerServiceDiscoveryTestHelper.CheckSessionApiV1(appManagerDiscoveredServices)
	if err != nil {
		return nil, err
	}

	err = appManagerServiceDiscoveryTestHelper.CheckRegistrationApiV1(appManagerDiscoveredServices)
	if err != nil {
		return nil, err
	}

	appManagementRepositoryLogWrapper := repository_log_wrappers.NewAppManagementRepositoryLogWrapper(
		uc.Logger,
		appManagerDiscoveredServices.RegistrationApi.V1,
	)

	registerAppResponse, err := appManagementRepositoryLogWrapper.RegisterApp(
		appManagerSessionToken,
		registerAppRequest,
	)
	if err != nil {
		return nil, err
	}

	defer func() {
		err = helpers.UnregisterApp(
			appManagementRepositoryLogWrapper,
			cryptographyTestHelper,
			appManagerSessionTestHelper,
			appKeyPair,
			appManager.PublicKey,
			registerAppResponse.RegistrationToken,
		)
	}()

	userIdentity := appManagementProcessModel.UserIdentity{
		Bsn: testInfo.Bsn,
	}

	_, err = appManagementRepositoryLogWrapper.LinkUserIdentity(
		registerAppResponse.RegistrationToken,
		userIdentity,
	)
	if err != nil {
		return nil, err
	}

	uc.Logger.Debug("appManager startSession")
	appManagerSessionToken, err = helpers.StartSession(
		cryptographyTestHelper,
		appManagerSessionTestHelper,
		appKeyPair.PublicKey,
		appKeyPair.PrivateKey,
		appManager.PublicKey,
	)
	if err != nil {
		return nil, err
	}

	fetchCertificateResponse, err := appManagementRepositoryLogWrapper.FetchCertificate(
		appManagerSessionToken,
		registerAppResponse.RegistrationToken,
	)
	if err != nil {
		return nil, err
	}

	certificate, err := cryptographyTestHelper.DecryptCertificate(
		fetchCertificateResponse.EncryptedCertificate,
		appKeyPair.PrivateKey,
	)
	if err != nil {
		return nil, err
	}

	serviceDiscoveryTestHelper := repository_log_wrappers.NewServiceDiscoveryTestHelper(uc.Logger, testInfo.DiscoveryUrl)

	discoveredServices, err := serviceDiscoveryTestHelper.FetchDiscoveredServices()
	if err != nil {
		return nil, err
	}

	err = serviceDiscoveryTestHelper.CheckDiscoveredServices(discoveredServices)
	if err != nil {
		return nil, err
	}

	err = serviceDiscoveryTestHelper.CheckSessionApiV1(discoveredServices)
	if err != nil {
		return nil, err
	}

	sessionTestHelper := repository_log_wrappers.NewSessionTestHelper(uc.Logger, discoveredServices.SessionApi.V1)

	sessionToken, err := helpers.StartSession(
		cryptographyTestHelper,
		sessionTestHelper,
		appKeyPair.PublicKey,
		appKeyPair.PrivateKey,
		testInfo.PublicKey,
	)
	if err != nil {
		return nil, err
	}

	document := citizenFinancialClaimModel.Document{
		Type: citizenFinancialClaimModel.DocumentType{
			Name: "FINANCIAL_CLAIMS_INFORMATION_REQUEST",
		},
	}

	documentSignature, err := cryptographyTestHelper.SignDocument(
		document,
		appKeyPair.PrivateKey,
	)
	if err != nil {
		return nil, err
	}

	envelope := citizenFinancialClaimModel.Envelope{
		Document:          document,
		DocumentSignature: documentSignature,
		Certificate:       certificate.Value,
		CertificateType:   certificate.Type,
	}

	encryptedEnvelope, err := cryptographyTestHelper.EncryptEnvelope(
		envelope,
		testInfo.PublicKey,
	)
	if err != nil {
		return nil, err
	}

	err = serviceDiscoveryTestHelper.CheckFinancialClaimRequestApiV3(discoveredServices)
	if err != nil {
		return nil, err
	}

	citizenFinancialClaimRepositoryLogWrapper := repository_log_wrappers.NewCitizenFinancialClaimRepositoryLogWrapper(uc.Logger, discoveredServices.FinancialClaimRequestApi.V3)

	configurationRequest := citizenFinancialClaimModel.ConfigurationRequest{
		EncryptedEnvelope: encryptedEnvelope,
	}

	configuration, err := citizenFinancialClaimRepositoryLogWrapper.ConfigureClaimsRequest(sessionToken, configurationRequest)
	if err != nil {
		return nil, err
	}

	financialClaimsInformation, err := citizenFinancialClaimRepositoryLogWrapper.RequestFinancialClaimsInformation(sessionToken, configuration.Token)
	if err != nil {
		return nil, err
	}

	financialClaimsInformationDocument, err := cryptographyTestHelper.DecryptFinancialClaimsInformationDocument(financialClaimsInformation.EncryptedFinancialClaimsInformationDocument, appKeyPair.PrivateKey)
	if err != nil {
		return nil, err
	}

	return financialClaimsInformationDocument, nil
}
