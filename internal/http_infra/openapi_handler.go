// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"log"
	"net/http"
	"os"
)

func handlerJson(responseWriter http.ResponseWriter, request *http.Request) {
	fileBytes, err := os.ReadFile("/api/openapi.json")
	if err != nil {
		panic(err)
	}
	responseWriter.WriteHeader(http.StatusOK)
	responseWriter.Header().Set("Content-Type", "application/json")
	_, err = responseWriter.Write(fileBytes)

	if err != nil {
		log.Printf("failed to write fileBytes: %v", err)
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
}

func handlerYaml(responseWriter http.ResponseWriter, request *http.Request) {
	fileBytes, err := os.ReadFile("/api/openapi.yaml")
	if err != nil {
		panic(err)
	}
	responseWriter.WriteHeader(http.StatusOK)
	responseWriter.Header().Set("Content-Type", "application/octet-stream")
	_, err = responseWriter.Write(fileBytes)

	if err != nil {
		log.Printf("failed to write fileBytes: %v", err)
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
}
