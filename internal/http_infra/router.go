// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"context"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"go.uber.org/zap"

	usecases "gitlab.com/blauwe-knop/experiments/implementation-test-process/pkg/usecase"
)

type key int

const (
	implementationTestUsecaseKey key = iota
	loggerKey                    key = iota
)

func NewRouter(implementationTestUsecase *usecases.ImplementationTestUsecase, logger *zap.Logger) *chi.Mux {
	r := chi.NewRouter()

	cors := cors.New(cors.Options{
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
	})

	r.Use(cors.Handler)

	r.Route("/v1", func(r chi.Router) {
		r.Use(middleware.SetHeader("API-Version", "1.0.0"))

		r.Route("/test", func(r chi.Router) {
			r.Use(middleware.Logger)

			// Note: post instead of get as bsn is being received with this request.
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), implementationTestUsecaseKey, implementationTestUsecase)

				ctx = context.WithValue(ctx, loggerKey, logger)

				handlerFunctionalOnlineTest(responseWriter, request.WithContext(ctx))
			})
		})

		r.Get("/openapi.json", func(responseWriter http.ResponseWriter, request *http.Request) {
			handlerJson(responseWriter, request)
		})

		r.Get("/openapi.yaml", func(responseWriter http.ResponseWriter, request *http.Request) {
			handlerYaml(responseWriter, request)
		})

		healthCheckHandler := healthcheck.NewHandler("implementation-test-process", []healthcheck.Checker{})
		r.Route("/health", func(r chi.Router) {
			r.Get("/", healthCheckHandler.HandleHealth)
			r.Get("/check", healthCheckHandler.HandlerHealthCheck)
		})
	})

	return r
}
