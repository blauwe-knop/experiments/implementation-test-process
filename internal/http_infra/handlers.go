// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/experiments/implementation-test-process/pkg/model"
	usecases "gitlab.com/blauwe-knop/experiments/implementation-test-process/pkg/usecase"
)

func handlerFunctionalOnlineTest(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	implementationTestUsecase, _ := context.Value(implementationTestUsecaseKey).(*usecases.ImplementationTestUsecase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	var testInfo model.TestInfo
	err := json.NewDecoder(request.Body).Decode(&testInfo)
	if err != nil {
		log.Printf("failed to decode request payload: %v", err)
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	financialClaimsInformationDocument, err := implementationTestUsecase.FunctionalOnlineTest(testInfo)

	if err != nil {
		logger.Error("function online test failed", zap.Error(err))
		http.Error(responseWriter, fmt.Sprintf("function online test failed, %v", err), http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(financialClaimsInformationDocument)
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
}
